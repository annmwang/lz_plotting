import ROOT
import argparse

def main():

    setstyle()

    ops = options()

    this_file_name = ops.i

    colors = [ROOT.kBlue-9]
    # ROOT.kMagenta-7,
    #           ROOT.kMagenta+2,
    #           ROOT.kOrange+1,
    #           ROOT.kBlue-9,
    #           ROOT.kBlue-4,
    #           ROOT.kBlue+2]
    
    canv = ROOT.TCanvas("canv","canv",800,800)
    canv.cd()
    files = {}
    files[this_file_name] = ROOT.TFile(ops.i)
    hist = "pulseArea_tpc_vs_AFT95mAFT5_SES2"
    full = files[this_file_name].Get(hist).ProjectionY()
    full.SetTitle("")
    full.GetXaxis().SetTitle("Pulse Area [phd]")
    full.GetYaxis().SetTitle("Pulses")
#    this_hist.GetYaxis().SetRangeUser(-10,30)
    full.SetFillColorAlpha(colors[0],0.4)
    full.SetLineColor(ROOT.kBlack)
    full.SetLineWidth(2)
    full.Rebin(2)
    full.Draw("hist")
#    flat_bkg = ROOT.TF1("bkg","[0]",500,1000)
#    full.Fit(flat_bkg,"R+")
#    full_spectrum = ROOT.TF1("full","[0]+gaus(1)+gaus(4)+gaus(7)+gaus(10)",0,1000)
    full_spectrum = ROOT.TF1("full","[0]+gaus(1)+gaus(4)+gaus(7)+gaus(10)+gaus(13)",0,1000)
    gaus0 = ROOT.TF1("gaus0","gaus",0,80)
    gaus1 = ROOT.TF1("gaus1","gaus",80,120)
    gaus2 = ROOT.TF1("gaus2","gaus",125,160)
    gaus3 = ROOT.TF1("gaus3","gaus",180,220)
    gaus4 = ROOT.TF1("gaus4","gaus",220,280)
    gaus4.SetParameter(0,1.)
    gaus4.SetParameter(1,250.)
    gaus4.SetParLimits(1,220.,280.)
    gaus4.SetParameter(2,10.)
    full.Fit(gaus0,"R+")
    full.Fit(gaus1,"R+")
    full.Fit(gaus2,"R+")
    full.Fit(gaus3,"R+")
    full.Fit(gaus4,"R+")
    # gaus0.Draw("same")
    # gaus1.Draw("same")
    # gaus2.Draw("same")
    # gaus3.Draw("same")
    # gaus4.Draw("same")
    full_spectrum.SetParameter(1,gaus0.GetParameter(0))
    full_spectrum.SetParameter(2,gaus0.GetParameter(1))
    full_spectrum.SetParameter(3,gaus0.GetParameter(2))

    full_spectrum.SetParameter(4,gaus1.GetParameter(0))
    full_spectrum.SetParameter(5,gaus1.GetParameter(1))
    full_spectrum.SetParameter(6,gaus1.GetParameter(2))

    full_spectrum.SetParameter(7,gaus2.GetParameter(0))
    full_spectrum.SetParameter(8,gaus2.GetParameter(1))
    full_spectrum.SetParameter(9,gaus2.GetParameter(2))

    full_spectrum.SetParameter(10,gaus3.GetParameter(0))
    full_spectrum.SetParameter(11,gaus3.GetParameter(1))
    full_spectrum.SetParameter(12,gaus3.GetParameter(2))

    full_spectrum.SetParLimits(10,gaus3.GetParameter(0)*0.5,gaus3.GetParameter(0)*1.5)
    full_spectrum.SetParLimits(11,gaus3.GetParameter(1)*0.5,gaus3.GetParameter(1)*1.5)
    full_spectrum.SetParLimits(12,gaus3.GetParameter(2)*0.5,gaus3.GetParameter(2)*1.5)

    full_spectrum.SetParameter(13,gaus4.GetParameter(0)/2)
    full_spectrum.SetParameter(14,gaus4.GetParameter(1))
    full_spectrum.SetParameter(15,gaus4.GetParameter(2))
    full_spectrum.SetParLimits(13,gaus4.GetParameter(0)*0.1,gaus4.GetParameter(0)*3)
    full_spectrum.SetParLimits(14,gaus4.GetParameter(1)*0.5,gaus4.GetParameter(1)*5)
    full_spectrum.SetParLimits(15,gaus4.GetParameter(2)*0.5,gaus4.GetParameter(2)*4)
    
    full.Fit(full_spectrum,"R+")
    full_spectrum.Draw("same")
    full_spectrum.SetNpx(500)

    fitlatex0 = ROOT.TLatex(0.6,0.8,"#mu_{0} = %.2f #pm %.2f phd"%(full_spectrum.GetParameter(2),full_spectrum.GetParError(2)))
    fitlatex0.SetNDC()
    fitlatex0.Draw("same")
    fitlatex0.SetTextSize(0.035)
    fitlatex1 = ROOT.TLatex(0.6,0.75,"#mu_{1} = %.2f #pm %.2f phd"%(full_spectrum.GetParameter(5),full_spectrum.GetParError(5)))
    fitlatex1.SetNDC()
    fitlatex1.Draw("same")
    fitlatex1.SetTextSize(0.035)
    fitlatex2 = ROOT.TLatex(0.6,0.7,"#mu_{2} = %.2f #pm %.2f phd"%(full_spectrum.GetParameter(8),full_spectrum.GetParError(8)))
    fitlatex2.SetNDC()
    fitlatex2.Draw("same")
    fitlatex2.SetTextSize(0.035)
    fitlatex3 = ROOT.TLatex(0.6,0.65,"#mu_{3} = %.2f #pm %.2f phd"%(full_spectrum.GetParameter(11),full_spectrum.GetParError(11)))
    fitlatex3.SetNDC()
    fitlatex3.Draw("same")
    fitlatex3.SetTextSize(0.035)
    fitlatex4 = ROOT.TLatex(0.6,0.6,"#mu_{4} = %.2f #pm %.2f phd"%(full_spectrum.GetParameter(14),full_spectrum.GetParError(14)))
    fitlatex4.SetNDC()
    fitlatex4.Draw("same")
    fitlatex4.SetTextSize(0.035)

    xlow = 26.5
    xhigh = 1000
    xwidth = full.GetBinWidth(1)
    
    gaus0_postfit = ROOT.TF1("gaus0_postfit","gaus",0,1000)
    gaus0_postfit.SetParameter(0,full_spectrum.GetParameter(1))
    gaus0_postfit.SetParameter(1,full_spectrum.GetParameter(2))
    gaus0_postfit.SetParameter(2,full_spectrum.GetParameter(3))
    gaus0_postfit.Draw("same")
    gaus0_postfit.SetLineColor(ROOT.kBlue)
    gaus0_postfit.SetLineWidth(1)

    
    gaus1_postfit = ROOT.TF1("gaus1_postfit","gaus",0,1000)
    gaus1_postfit.SetParameter(0,full_spectrum.GetParameter(4))
    gaus1_postfit.SetParameter(1,full_spectrum.GetParameter(5))
    gaus1_postfit.SetParameter(2,full_spectrum.GetParameter(6))
    gaus1_postfit.Draw("same")
    gaus1_postfit.SetLineColor(ROOT.kBlue)
    gaus1_postfit.SetLineWidth(1)

    
    gaus2_postfit = ROOT.TF1("gaus2_postfit","gaus",0,1000)
    gaus2_postfit.SetParameter(0,full_spectrum.GetParameter(7))
    gaus2_postfit.SetParameter(1,full_spectrum.GetParameter(8))
    gaus2_postfit.SetParameter(2,full_spectrum.GetParameter(9))
    gaus2_postfit.Draw("same")
    gaus2_postfit.SetLineColor(ROOT.kBlue)
    gaus2_postfit.SetLineWidth(1)
    
    gaus3_postfit = ROOT.TF1("gaus3_postfit","gaus",0,1000)
    gaus3_postfit.SetParameter(0,full_spectrum.GetParameter(10))
    gaus3_postfit.SetParameter(1,full_spectrum.GetParameter(11))
    gaus3_postfit.SetParameter(2,full_spectrum.GetParameter(12))
    gaus3_postfit.Draw("same")
    gaus3_postfit.SetLineColor(ROOT.kBlue)
    gaus3_postfit.SetLineWidth(1)

    gaus4_postfit = ROOT.TF1("gaus4_postfit","gaus",0,1000)
    gaus4_postfit.SetParameter(0,full_spectrum.GetParameter(13))
    gaus4_postfit.SetParameter(1,full_spectrum.GetParameter(14))
    gaus4_postfit.SetParameter(2,full_spectrum.GetParameter(15))
    gaus4_postfit.Draw("same")
    gaus4_postfit.SetLineColor(ROOT.kBlue)
    gaus4_postfit.SetLineWidth(1)
    
    flat_bkg_postfit = ROOT.TF1("bkg_postfit","[0]",500,1000)
    flat_bkg_postfit.SetParameter(0,full_spectrum.GetParameter(0))
    flat_bkg_postfit.Draw("same")
    flat_bkg_postfit.SetLineColor(ROOT.kBlue)


    fr_gaus0 = (gaus0_postfit.Integral(xlow,xhigh)/xwidth) / full.Integral()
    fr_gaus1 = (gaus1_postfit.Integral(xlow,xhigh)/xwidth) / full.Integral()
    fr_gaus2 = (gaus2_postfit.Integral(xlow,xhigh)/xwidth) / full.Integral()
    fr_gaus3 = (gaus3_postfit.Integral(xlow,xhigh)/xwidth) / full.Integral()
    fr_gaus4 = (gaus4_postfit.Integral(xlow,xhigh)/xwidth) / full.Integral()
    fr_flat_bkg = (flat_bkg_postfit.Integral(xlow,xhigh)/xwidth) / full.Integral()

    print(fr_gaus0,fr_gaus1,fr_gaus2,fr_gaus3,fr_gaus4,fr_flat_bkg)
    print(fr_gaus0+fr_gaus1+fr_gaus2+fr_gaus3+fr_gaus4+fr_flat_bkg)
    print(fr_flat_bkg)
    #full.GetXaxis().SetRangeUser(0,500)
    canv.Print("%s/pulse_area.pdf"%(ops.odir))
    canv.SetLogy(1)
    canv.Print("%s/pulse_area_log.pdf"%(ops.odir))
    
def setstyle():
    ROOT.gROOT.SetBatch()
    ROOT.gStyle.SetStatY(0.85)
    ROOT.gStyle.SetPadTopMargin(0.1)
    ROOT.gStyle.SetPadRightMargin(0.05)
    ROOT.gStyle.SetPadBottomMargin(0.12)
    ROOT.gStyle.SetPadLeftMargin(0.15)
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
    ROOT.gStyle.SetPaintTextFormat(".2f")
    ROOT.gStyle.SetTextFont(42)
    ROOT.gStyle.SetOptStat(0)

def options():
    parser = argparse.ArgumentParser(usage=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", help="Input data file")
    parser.add_argument("--odir", help="Output directory")
    return parser.parse_args()

if __name__=="__main__":
    main()
