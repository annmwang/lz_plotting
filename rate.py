import ROOT
import argparse
import math

def main():

    setstyle()

    ops = options()

    this_file_name = ops.i
#    run_num = int(ops.r)

    colors = [ROOT.kBlue-9]
    # ROOT.kMagenta-7,
    #           ROOT.kMagenta+2,
    #           ROOT.kOrange+1,
    #           ROOT.kBlue-9,
    #           ROOT.kBlue-4,
    #           ROOT.kBlue+2]
    
    hists = ["S2SE_triggerTime",
             "S2SE_triggerTime_emitter0_large",
             "S2SE_triggerTime_emitter1_large",
             ]

    files = {}

    livetime = 2.5*pow(10,-3) # 2.5 ms (this is valid when using part of the window and cutting out t < 0)
    
    for hist in hists:
        canv = ROOT.TCanvas("canv","canv",1200,800)
        canv.cd()
        canv.SetLogy(1)
        files[this_file_name] = ROOT.TFile(this_file_name)
        this_hist = files[this_file_name].Get(hist)
        ntrig_hist = files[this_file_name].Get("triggerTime_random")
        # if hist != "SES2_triggerTime":
        this_hist.Rebin(6)
        ntrig_hist.Rebin(6)
        nbins = ntrig_hist.GetNbinsX()
        xlow = ntrig_hist.GetBinLowEdge(1)
        xhigh = ntrig_hist.GetBinLowEdge(nbins)+ntrig_hist.GetBinWidth(nbins)
        rate_hist = ROOT.TH1D("rate_hist",";Time since first trigger [s];Rate [Hz]",nbins,xlow,xhigh)
        for ibin in range(1,ntrig_hist.GetNbinsX()+1):
            bin_time = ntrig_hist.GetBinContent(ibin)*livetime
            print(bin_time)
            if bin_time > pow(10,-20):
                bin_rate = this_hist.GetBinContent(ibin)/bin_time
                bin_err  = math.sqrt(this_hist.GetBinContent(ibin))/bin_time
            else:
                bin_rate = 0
                bin_err  = 0
            rate_hist.SetBinContent(ibin, bin_rate)
            rate_hist.SetBinError  (ibin,bin_err)
        rate_hist.Draw("hist same")
        rate_hist.SetTitle("")
        rate_hist.GetYaxis().SetTitle("Rate [Hz]")
        rate_hist.SetFillColorAlpha(colors[0],0.4)
        rate_hist.SetMarkerColor(colors[0])
        rate_hist.SetMarkerStyle(20)
        rate_hist.SetMarkerSize(0.8)
        rate_hist.SetLineColor(ROOT.kBlack)
        rate_hist.SetLineWidth(2)
        canv.Print("%s/rate_%s_log.pdf"%(ops.odir,hist))
        canv.SetLogy(0)
        canv.Print("%s/rate_%s.pdf"%(ops.odir,hist))

def setstyle():
    ROOT.gROOT.SetBatch()
    ROOT.gStyle.SetStatY(0.85)
    ROOT.gStyle.SetPadTopMargin(0.1)
    ROOT.gStyle.SetPadRightMargin(0.1)
    ROOT.gStyle.SetPadBottomMargin(0.12)
    ROOT.gStyle.SetPadLeftMargin(0.15)
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
    ROOT.gStyle.SetPaintTextFormat(".2f")
    ROOT.gStyle.SetTextFont(42)
    ROOT.gStyle.SetOptStat(0)

def options():
    parser = argparse.ArgumentParser(usage=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", help="Input data file")
    parser.add_argument("--odir", help="Output directory")
    return parser.parse_args()

if __name__=="__main__":
    main()
