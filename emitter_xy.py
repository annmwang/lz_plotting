import ROOT
import argparse

def main():

    setstyle()

    ops = options()

    this_file_name = ops.i

    colors = [ROOT.kBlue-9]
    # ROOT.kMagenta-7,
    #           ROOT.kMagenta+2,
    #           ROOT.kOrange+1,
    #           ROOT.kBlue-9,
    #           ROOT.kBlue-4,
    #           ROOT.kBlue+2]
    
    canv = ROOT.TCanvas("canv","canv",800,800)
    canv.cd()
    files = {}
    files[this_file_name] = ROOT.TFile(ops.i)
    hist = "SE_Y_vs_X_short"
    this_hist = files[this_file_name].Get(hist)
    this_hist.SetTitle("")
    this_hist.GetXaxis().SetRangeUser(-100,-25)
    this_hist.GetYaxis().SetRangeUser(-10,30)
    px = this_hist.ProjectionX()
    py = this_hist.ProjectionY()
    px.SetFillColorAlpha(colors[0],0.4)
    px.SetLineColor(ROOT.kBlack)
    px.SetLineWidth(2)
    px.Draw("hist")
    canv.Print("%s/%s_px.pdf"%(ops.odir,hist))
    py.SetFillColorAlpha(colors[0],0.4)
    py.SetLineColor(ROOT.kBlack)
    py.SetLineWidth(2)
    py.Draw("hist")
    canv.Print("%s/%s_py.pdf"%(ops.odir,hist))
    
def setstyle():
    ROOT.gROOT.SetBatch()
    ROOT.gStyle.SetStatY(0.85)
    ROOT.gStyle.SetPadTopMargin(0.1)
    ROOT.gStyle.SetPadRightMargin(0.05)
    ROOT.gStyle.SetPadBottomMargin(0.12)
    ROOT.gStyle.SetPadLeftMargin(0.15)
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
    ROOT.gStyle.SetPaintTextFormat(".2f")
    ROOT.gStyle.SetTextFont(42)
    ROOT.gStyle.SetOptStat(0)

def options():
    parser = argparse.ArgumentParser(usage=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", help="Input data file")
    parser.add_argument("--odir", help="Output directory")
    return parser.parse_args()

if __name__=="__main__":
    main()
